import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/service/auth.service';
import { ToastrService } from 'ngx-toastr';
import { HttpClient } from '@angular/common/http';



@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent {
  constructor(private builder: FormBuilder, private router: Router,
    private service: AuthService, private toastr: ToastrService, public http: HttpClient) {
  }

  noofemployees: any;
  howdoyouknow: any;
  


  registerForm: FormGroup = this.builder.group({
    contact_person_name: this.builder.control('', Validators.compose([Validators.required, Validators.minLength(4)])),
    company_name: this.builder.control('', Validators.compose([Validators.required, Validators.minLength(2)])),
    email: this.builder.control('', Validators.compose([Validators.required, Validators.email])),
    contact_number: this.builder.control('', Validators.compose([Validators.required, Validators.minLength(4)])),   // Validators.pattern(/^\d{10}$/)
    no_of_employees: this.builder.control(''),
    how_do_you_know_us: this.builder.control(''),
    isActive: this.builder.control(false)
  });

  proceedRegistration() {
    if (this.registerForm.valid) {
      this.service.ProceedRegister(this.registerForm.value).subscribe(res => {
        this.toastr.success('Registration Successful');
        this.router.navigate(['/home']);
      });
    } else {
      this.toastr.warning("Please enter valid data");
    }
    }
}



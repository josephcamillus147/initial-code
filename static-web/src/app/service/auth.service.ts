import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }
  apiurl='https://5p7qpwhqck.execute-api.eu-west-2.amazonaws.com/api/';

  GetAll(){
    return this.http.get(this.apiurl);
  }

  GetById(id:any){
    return this.http.get(this.apiurl+"/"+id);
  }

  ProceedRegister(inputdata: any) {
    return this.http.post(this.apiurl, inputdata);
  }
}